from django.contrib import admin
from django.contrib import admin
from core.models import Teacher, Student, Group, Course
# Register your models here.

admin.site.register(Teacher)
admin.site.register(Student)
admin.site.register(Group)
admin.site.register(Course)