from core.models import Group

def get_all_groups(request):
    return {
        'groups': Group.objects.all()
    }
