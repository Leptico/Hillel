from django import forms
from django.contrib.auth.models import User
from django.core import validators
from django.core.validators import MaxLengthValidator
from core.models import Student, Course


class CreateCourseForm(forms.ModelForm):

    class Meta:
        model = Course
        fields = "__all__"


    def clean_name(self):
        name = self.cleaned_data["name"]
        if Course.objects.filter(name__iexact=name):
            raise forms.ValidationError("Name of the course is not unique")
        return name




class CreateStudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = "__all__"
    def save(self, *args, **kwargs):
        student = super(CreateStudentForm, self).save(*args, **kwargs)
        student.save()
        return student
    def clean_name(self):
        name = self.cleaned_data["name"]
        if Student.objects.filter(name__iexact=name):
            raise forms.ValidationError("Student name is not unique")

        return name

# class LoginForm(forms.Form):
#     username = forms.CharField()
#     password = forms.CharField(widget=forms.PasswordInput())
#
#     def clean(self):
#         self.user = User.objects.filter(username=self.cleaned_data['username']).first()
#         if self.user and self.user.check_password(self.cleaned_data['password']):
#             return self.cleaned_data
#         else:
#             raise forms.ValidationError('Username or password is wrong!')
