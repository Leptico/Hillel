from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views import View
from django.db.models import Q, F
from django.shortcuts import redirect
from django.views.generic import TemplateView, ListView, FormView, CreateView, UpdateView

from django.views.generic import TemplateView
from django.db.models import Prefetch
from core.models import Teacher, Student, Group
from core.models import Course
from core.forms import CreateStudentForm, CreateCourseForm




class IndexView(ListView):
    model = Course
    template_name = 'index.html'
    paginate_by = 5

    #
    def get_queryset(self):

        queryset = super(IndexView, self).get_queryset().filter(teacher__isnull=False)
        return queryset.select_related(
            "teacher"
        ).prefetch_related(
            "group",
        )

    def get_context_data(self, *args, **kwargs):
        context = super(IndexView, self).get_context_data(*args, **kwargs)

        context['groups'] = Group.objects.all()
        context['teacher'] = Teacher.objects.all()
        return context


class NavView(ListView):
    model = Course
    template_name = 'index.html'
    paginate_by = 5

    def get_queryset(self):
        queryset = super(NavView, self).get_queryset().filter(group__name=self.kwargs["group_name"])
        return queryset.select_related(
            "teacher"
        ).prefetch_related(
            "group"
        )
class AllStudentsView(ListView):
    model = Student
    template_name = "all_students.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ListView, self).get_context_data(*args, **kwargs)
        context["group"] = Group.objects.all()
        return context


class CreateCourse(CreateView):
    template_name = "create_course.html"
    model = Course
    form_class = CreateCourseForm
    success_url = "/"


class Createstudent(CreateView):
    template_name = "create_student.html"
    success_url = "/"
    form_class = CreateStudentForm

    def form_valid(self, form):
        form.save()
        return super(Createstudent, self).form_valid(form)


class EditCourseView(FormView):
    template_name = "create_course.html"
    form_class = CreateCourseForm
    success_url = '/'

    def get_form_kwargs(self):
        form_kwargs = super(EditCourseView, self).get_form_kwargs()
        form_kwargs["instance"] = get_object_or_404(Course, id=self.kwargs["course_id"])
        return form_kwargs

    def form_valid(self, form):
        form.save()
        return super(EditCourseView, self).form_valid(form)


class EditStudentView(FormView):
    template_name = "create_student.html"
    form_class = CreateStudentForm
    success_url = "/"

    def get_form_kwargs(self):
        form_kwargs = super(EditStudentView, self).get_form_kwargs()
        form_kwargs["instance"] = get_object_or_404(Student, id=self.kwargs["student_id"])
        return form_kwargs

    def form_valid(self, form):
        form.save()
        return super(EditStudentView, self).form_valid(form)


class SearchView(ListView):
    template_name = "search.html"
    model = Course
    paginate_by = 5


    def get_queryset(self):
        query = self.request.GET.get('q', None)
        if query:
            return self.model.objects.filter(
                Q(name__icontains=query) |
                Q(group__name__icontains=query) |
                Q(teacher__name__icontains=query)
            )
        return super(SearchView, self).get_queryset()

    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(**kwargs)
        data["searched"] = self.request.GET["q"]
        return data


