from django.db import models


# Create your models here.



class Student(models.Model):
    name = models.CharField(max_length=255)
    age = models.PositiveIntegerField(default=0)
    email = models.CharField(max_length=255)
    group = models.ManyToManyField("core.Group")

    def __str__(self):
        return self.name


class Course(models.Model):
    name = models.CharField(max_length=255)
    group = models.ManyToManyField("core.Group")
    teacher = models.ForeignKey("core.Teacher", on_delete=models.SET_NULL, null=True)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.name


#on_delete=models.SET_NULL, null=True
class Teacher(models.Model):
    name = models.CharField(max_length=255)
    age = models.PositiveIntegerField(default=0)
    email = models.CharField(max_length=255)
    group = models.ManyToManyField("core.Group")
    def __str__(self):
        return self.name


class Group(models.Model):
    name = models.CharField(max_length=255)
    def __str__(self):
        return self.name




