from django import template
import os
from core.models import Course

register = template.Library()

@register.inclusion_tag("includes/course_loop.html")
def get_five():
    return {
        "course_list": Course.objects.all().order_by("?")[:7]
    }