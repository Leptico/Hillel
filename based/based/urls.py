"""djangoProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, include
from core import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", views.IndexView.as_view()),
    path("create_student/", views.Createstudent.as_view()),
    path("course/edit/<int:course_id>/", views.EditCourseView.as_view(), name="course_edit"),
    path("create_course/", views.CreateCourse.as_view()),
    path("all_students/", views.AllStudentsView.as_view(), name="students"),
    path("student/edit/<int:student_id>/", views.EditStudentView.as_view(), name="student_edit"),
    path("group_name=<str:group_name>/", views.NavView.as_view(), name="levels"),
    path("login/", LoginView.as_view(template_name='login.html'), name='login'),
    path("logout/", LogoutView.as_view(), name='logout'),
    path("search/", views.SearchView.as_view(), name='search'),


    path('__debug__/', include('debug_toolbar.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
